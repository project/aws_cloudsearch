<?php

namespace Drupal\aws_cloudsearch\Helper;

use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Core\Entity\ContentEntityType;

/**
 * To change this license header, choose License Headers in Project Properties.
 */
class AwsHelper {

  /**
   * Drupal\Core\Database\Driver\mysql\Connection definition.
   *
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;
  /**
   * Entity manager handler.
   *
   * @var Drupal\Core\Entity\EntityManager
   */
  private $entityManager;
  private $entityTypeManager;
  private $entityFieldManager;

  /**
   * Class instance.
   *
   * @var object
   */
  public static $awsHelper = NULL;

  /**
   * Allow entity to configure aws serach.
   *
   * @var array
   */
  public $allowedEntity = [];
  /**
   * Add fields that is not required to ingest.
   */
  public $notAllowedFields = [];
  /**
   * Get class instance using this function.
   *
   * @return object
   *    Return instance.
   */
  public static function getInstance() {
    if (!self::$awsHelper) {
      self::$awsHelper = new AwsHelper();
    }
    return self::$awsHelper;
  }

  /**
   * Constructs a new AwsConfig object.
   */
  public function __construct($entityManager = NULL) {
    $container = \Drupal::getContainer();
    $this->entityManager = $container->get('entity.manager');
    $this->entityTypeManager = $container->get('entity_type.manager');
    $this->database = $container->get('database');
    $this->entityFieldManager = $container->get('entity_field.manager');
    // Add allowed entity type.
    $this->allowedEntity['node'] = 'Content';
    $this->allowedEntity['taxonomy_term'] = 'Taxonomy';
    $this->allowedEntity['field_collection_item'] = 'Field Collection';
    $this->allowedEntity['paragraph'] = 'Paragraphs';
    // Add fields that are not required to ingest.
    $this->notAllowedFields[] = 'id';
    $this->notAllowedFields[] = 'uuid';
    $this->notAllowedFields[] = 'revision_id';
    $this->notAllowedFields[] = 'type';
    $this->notAllowedFields[] = 'langcode';
    $this->notAllowedFields[] = 'uid';
    $this->notAllowedFields[] = 'created';
    $this->notAllowedFields[] = 'revision_uid';
    $this->notAllowedFields[] = 'parent_id';
    $this->notAllowedFields[] = 'parent_type';
    $this->notAllowedFields[] = 'behavior_settings';
    $this->notAllowedFields[] = 'default_langcode';
    $this->notAllowedFields[] = 'revision_translation_affected';
    $this->notAllowedFields[] = 'nid';
    $this->notAllowedFields[] = 'vid';
    $this->notAllowedFields[] = 'revision_timestamp';
    $this->notAllowedFields[] = 'revision_log';
    $this->notAllowedFields[] = 'changed';
    $this->notAllowedFields[] = 'promote';
    $this->notAllowedFields[] = 'sticky';
    $this->notAllowedFields[] = 'comment';
    $this->notAllowedFields[] = 'item_id';
    $this->notAllowedFields[] = 'host_type';
    $this->notAllowedFields[] = 'tid';
    $this->notAllowedFields[] = 'parent';
    $this->notAllowedFields[] = 'weight';
    $this->notAllowedFields[] = 'parent_field_name';
  }

  /**
   * Get bundles list.
   *
   * @param string $entity
   *    Entity type.
   *
   * @return array
   *    Return array of machine name and label.
   */
  public function getBundles($entity = NULL) {
    $bundles = [];
    switch ($entity) {
      case 'node':
        $types = $this->entityManager->getStorage('node_type')
          ->loadMultiple();
        if (count($types) > 0) {
          foreach ($types as $cType) {
            $bundles[$cType->get('type')] = $cType->get('name');
          }
        }
        break;

      case 'field_collection_item':
        $collection = $this->entityManager->getStorage('field_collection')->loadMultiple();
        foreach ($collection as $cType) {
          $bundles[$cType->get('id')] = $cType->get('label');
        }
        break;

      case 'paragraph':
        $collection = $this->entityManager->getStorage('paragraphs_type')->loadMultiple();
        foreach ($collection as $cType) {
          $bundles[$cType->get('id')] = $cType->get('label');
        }
        break;

      case 'taxonomy_term':
        $collection = $this->entityManager->getStorage('taxonomy_vocabulary')->loadMultiple();
        foreach ($collection as $cType) {
          $bundles[$cType->get('vid')] = $cType->get('name');
        }
        break;

      default:
        break;
    }
    return $bundles;
  }

  /**
   * Get vocabulary list.
   *
   * @return array
   *    Return array of machine name and label.
   */
  public function getVocabulary($type = NULL) {
    $vocabularies = Vocabulary::loadMultiple();
    $vocabulariesList = [];
    foreach ($vocabularies as $vid => $vocablary) {
      $vocabulariesList[$vid] = $vocablary->get('name');
    }
    return $vocabulariesList;
  }

  /**
   * Get Datasource.
   *
   * @return array
   *    Return array of machine name and label.
   */
  public function getDataSource() {
    $entityDefinations = $this->entityTypeManager->getDefinitions();
    /* @var $definition EntityTypeInterface */
    foreach ($entityDefinations as $definition) {
      if ($definition instanceof ContentEntityType) {
        $machineName = $definition->get('id');
        if (array_key_exists($machineName, $this->allowedEntity)) {
          $entityType = $definition->getBundleEntityType();
          $label = $definition->getCollectionLabel()->__toString();
          $label = str_replace(' entities', '', $label);
          if (empty($label)) {
            $label = $definition->getBundleLabel();
          }
          if (!empty($machineName)) {
            $datasourceArr[$machineName] = $label . ' (Entity Type : ' . $entityType . ')';
          }
        }
      }
    }
    $this->removeDataSource($datasourceArr);
    asort($datasourceArr);
    return $datasourceArr;
  }

  /**
   * Get the entity label from AllowedEntity array.
   *
   * @param string $type
   *    Entity type.
   *
   * @return string
   *    Return entity label if exists, return Type after removing (_) to space.
   */
  public function getEntityLabel($type) {
    if (isset($this->allowedEntity[$type])) {
      return $this->allowedEntity[$type];
    }
    else {
      return ucwords(str_replace('_', ' ', $type));
    }
  }

  /**
   * Remove datasource that is unsed.
   *
   * @param array $datasourceArr
   *    Datasource array as reference object.
   */
  public function removeDataSource(&$datasourceArr) {
    $entityList = ['shortcut_set'];
    foreach ($datasourceArr as $machine_name => $label) {
      if (in_array($machine_name, $entityList)) {
        unset($datasourceArr[$machine_name]);
      }
    }
  }

  /**
   * Create machine name. Replace all characters except alpha & number.
   *
   * @param string $name
   *   Name will check and replace the string.
   *
   * @return string
   *   It will return the machine name.
   */
  public function createMachineName($name) {
    $name = preg_replace('/[^a-zA-Z0-9_ ]/', '', strtolower(trim($name)));
    $name = preg_replace('/[_]+/', '_', preg_replace('/\s+/', ' ', $name));
    $name = ltrim(rtrim($name, '_'), '_');
    $name = preg_replace('/[^a-zA-Z0-9]/', '_', $name);
    if (strlen($name) > 50) {
      $name = substr($name, 0, 50);
    }
    return $name;
  }

  /**
   * Get selected checkboxex values.
   *
   * @param array $values
   *    Checkboxes values.
   *
   * @return array
   *    Return array.
   */
  public function getSelectedTypes($values) {
    $return = [];
    if (!empty($values)) {
      foreach ($values as $entityType => $entityValues) {
        if (!empty($entityValues)) {
          $return[$entityType] = $entityValues;
        }
      }
    }
    return $return;
  }

  /**
   * Create aws domain name.
   *
   * @param array $post
   *    Data in array.
   *
   * @return array
   *    Return status with message.
   */
  public function saveDomain(array $post) {
    // Check name field is empty or not.
    if (empty($post['name'])) {
      $message = t('Required field can not empty.');
      return ['status' => 'error', 'message' => $message];
    }
    $name = $post['name'];
    // Check domain already exists or not.
    if ($this->getDomains(['name' => $name]) != FALSE) {
      $message = t('Domain name already exists.');
      return ['status' => 'error', 'message' => $message];
    }
    try {
      $this->database->insert('aws_domain')
            ->fields([
              'name' => trim($name),
              'status' => 1,
              'created_at' => time(),
              'updated_at' => time(),
            ])->execute();
      $message = t('Domain <strong>@name</strong> added successfully.', ['@name' => $name]);
      return ['status' => 'success', 'message' => $message];
    }
    catch (\Exception $e) {
      return ['status' => 'error', 'message' => $e->getMessage()];
    }
  }

  /**
   * Update aws domain name.
   *
   * @param array $post
   *    Data in array.
   *
   * @return array
   *    Return status with message.
   */
  public function updateDomain(array $post, $id) {
    // Check name, id and status field is empty or not.
    if ((empty($post['name'])) || (empty($id))) {
      $message = t('Required field can not empty.');
      return ['status' => 'error', 'message' => $message];
    }
    $name   = $post['name'];
    // Check domain already exists or not.
    if ($this->getDomains(['name' => $name, 'not_id' => $id]) != FALSE) {
      $message = t('Domain name already exists.');
      return ['status' => 'error', 'message' => $message];
    }
    $status = (isset($post['status'])) ? $post['status'] : 0;
    try {
      $this->database->update('aws_domain')
            ->fields([
              'name' => trim($name),
              'status' => $status,
              'updated_at' => time(),
            ])->condition('id', $id)->execute();
      $message = t('Domain <strong>@name</strong> updated successfully.', ['@name' => $name]);
      return ['status' => 'success', 'message' => $message];
    }
    catch (\Exception $e) {
      return ['status' => 'error', 'message' => $e->getMessage()];
    }
  }

  /**
   * Fetch aws domain on different filter.
   *
   * @param array $args
   *    Arguments value with key.
   *
   * @return object
   *    Result set.
   */
  public function getDomains(array $args = NULL) {
    $query = $this->database->select('aws_domain', 'ad');
    $query->fields('ad', [
      'id',
      'name',
      'status',
      'created_at',
      'updated_at',
    ]);
    if (!empty($args['id'])) {
      $query->condition('ad.id', $args['id']);
    }
    if (!empty($args['not_id'])) {
      $query->condition('ad.id', $args['not_id'], '!=');
    }
    if (!empty($args['name'])) {
      $query->condition('ad.name', $args['name']);
    }
    // To fetch single record.
    if (!empty($args['id'])) {
      $result = $query->execute()->fetch();
    }
    else {
      // To fetch multiple records.
      $result = $query->execute()->fetchAll();
    }
    return $result;
  }

  /**
   * Delete domain by ID.
   *
   * @param int $id
   *    Domain id.
   *
   * @return bool
   *    Return true.
   */
  public function deleteDomain($id) {
    $this->database->delete('aws_domain')
            ->condition('id', $id)->execute();
    return TRUE;
  }

  /**
   * Create Index name.
   *
   * @param array $post
   *    Data in array.
   *
   * @return array
   *    Return status with message.
   */
  public function saveIndex(array $post) {
    // Check required field is empty or not.
    if ((empty($post['domain_id'])) || (empty($post['name'])) || (empty($post['entities']))) {
      $message = t('Required field can not empty.');
      return ['status' => 'error', 'message' => $message];
    }
    $domain_id       = $post['domain_id'];
    $name         = $post['name'];
    // Check index name already exists or not.
    if ($this->getIndexes(['name' => $name]) != FALSE) {
      $message = t('Index name already exists.');
      return ['status' => 'error', 'message' => $message];
    }
    try {
      $id = $this->database->insert('aws_index')
        ->fields([
          'domain_id' => $domain_id,
          'name' => trim($name),
          'status' => 1,
          'created_at' => time(),
          'updated_at' => time(),
        ])->execute();
      // On success.
      if (!empty($id)) {
        $post['index_id'] = $id;
        // Save index data [Entity, bundles].
        $this->saveIndexData($post);
      }
      $message = t('Index <strong>@name</strong> added successfully.', ['@name' => $name]);
      return ['status' => 'success', 'message' => $message];
    }
    catch (\Exception $e) {
      return ['status' => 'error', 'message' => $e->getMessage()];
    }
  }

  /**
   * Update Index.
   *
   * @param array $post
   *    Data in array.
   * @param int $id
   *    Index id.
   *
   * @return array
   *    Return status with message.
   */
  public function updateIndex(array $post, $id) {
    // Check required field is empty or not.
    if ((empty($post['domain_id'])) || (empty($post['name'])) || (empty($id)) || (empty($post['entities']))) {
      $message = t('Required field can not empty.');
      return ['status' => 'error', 'message' => $message];
    }
    $domain_id    = $post['domain_id'];
    $name         = $post['name'];
    // Check index name already exists or not.
    if ($this->getIndexes(['name' => $name, 'not_id' => $id]) != FALSE) {
      $message = t('Index name already exists.');
      return ['status' => 'error', 'message' => $message];
    }
    try {
      $this->database->update('aws_index')
        ->fields([
          'domain_id' => $domain_id,
          'name' => trim($name),
          'status' => 1,
          'updated_at' => time(),
        ])->condition('id', $id)->execute();
      // On success.
      $post['index_id'] = $id;
      // Save index data [Entity, bundles].
      $this->updateIndexData($post, $id);
      $message = t('Index <strong>@name</strong> updated successfully.', ['@name' => $name]);
      return ['status' => 'success', 'message' => $message];
    }
    catch (\Exception $e) {
      return ['status' => 'error', 'message' => $e->getMessage()];
    }
  }

  /**
   * Delete index by ID.
   *
   * @param int $id
   *    Index id.
   *
   * @return bool
   *    Return true.
   */
  public function deleteIndex($id) {
    $this->database->delete('aws_index')
            ->condition('id', $id)->execute();
    // Delete index data by index id.
    $this->deleteIndexData($id);
    return TRUE;
  }

  /**
   * Fetch aws Index on different filter.
   *
   * @param array $args
   *    Arguments value with key.
   *
   * @return object
   *    Result set.
   */
  public function getIndexes(array $args = NULL) {
    $query = $this->database->select('aws_index', 'a');
    $query->fields('a', [
      'id',
      'domain_id',
      'name',
      'status',
      'created_at',
      'updated_at',
    ]);
    if (!empty($args['id'])) {
      $query->condition('a.id', $args['id']);
    }
    if (!empty($args['not_id'])) {
      $query->condition('a.id', $args['not_id'], '!=');
    }
    if (!empty($args['name'])) {
      $query->condition('a.name', $args['name']);
    }
    // To fetch single record.
    if (!empty($args['id'])) {
      $result = $query->execute()->fetch();
    }
    else {
      // To fetch multiple records.
      $result = $query->execute()->fetchAll();
    }
    return $result;
  }

  /**
   * Create Index data.
   *
   * @param array $post
   *    Data in array.
   *
   * @return bool
   *    Return TRUE or FALSE.
   */
  public function saveIndexData(array $post) {
    // Check required field is empty or not.
    if ((empty($post['index_id'])) || (empty($post['entities']))) {
      return FALSE;
    }
    $ids = [];
    $index_id = $post['index_id'];
    $entity   = $post['entities'];
    try {
      foreach ($post['entities'] as $entity => $bundles) {
        // Remove the empty entity that have value as 0.
        $bundles = $this->getSelectedTypes($bundles);
        $ids[] = $this->database->insert('aws_index_data')
          ->fields([
            'index_id' => $index_id,
            'entity' => trim($entity),
            'bundles' => trim(serialize($bundles)),
            'created_at' => time(),
            'updated_at' => time(),
          ])->execute();
      }
      return $ids;
    }
    catch (\Exception $e) {
      return FALSE;
    }
  }

  /**
   * Update Index data.
   *
   * @param array $post
   *    Data in array.
   * @param int $index_id
   *    Index id.
   *
   * @return array
   *    Return TRUE or FALSE.
   */
  public function updateIndexData(array $post, $index_id) {
    // Check required field is empty or not.
    if ((empty($index_id)) || (empty($post['entities']))) {
      return FALSE;
    }
    $post['index_id'] = $index_id;
    try {
      $indexDataId = [];
      foreach ($post['entities'] as $entity => $bundles) {
        $indexResults = $this->getIndexData(['index_id' => $index_id, 'entity' => $entity]);
        // If entity already exists update entity, bundles.
        if ($indexResults) {
          // Keep the updated Id into $indexDataId variable.
          $indexDataId[] = $indexResults[0]->id;
          // Remove the empty entity that have value as 0.
          $bundles = $this->getSelectedTypes($bundles);
          $this->database->update('aws_index_data')
            ->fields([
              'entity' => trim($entity),
              'bundles' => trim(serialize($bundles)),
              'created_at' => time(),
            ])->condition('index_id', $index_id)->condition('entity', $entity)->execute();
        }
        else {
          // If new entity selected, insert into table.
          $newIndexData = ['index_id' => $index_id, 'entities' => [$entity => $bundles]];
          // Merge the new inserted ids into $indexDataId variable.
          $ids = $this->saveIndexData($newIndexData);
          if (count($ids) > 0) {
            $indexDataId = array_merge($indexDataId, $ids);
          }
        }
      }
      // Delete those ID that are not updated or inserted.
      if (count($indexDataId) > 0) {
        $this->database->delete('aws_index_data')
            ->condition('index_id', $index_id)->condition('id', $indexDataId, 'not in')->execute();
      }
      return TRUE;
    }
    catch (\Exception $e) {
      return FALSE;
    }
  }

  /**
   * Delete Index data by index id.
   *
   * @param int $index_id
   *    Index id.
   *
   * @return bool
   *    Return true.
   */
  public function deleteIndexData($index_id) {
    $this->database->delete('aws_index_data')
            ->condition('index_id', $index_id)->execute();
    return TRUE;
  }

  /**
   * Fetch aws Index on different filter.
   *
   * @param array $args
   *    Arguments value with key.
   *
   * @return object
   *    Result set.
   */
  public function getIndexData(array $args = NULL) {
    $query = $this->database->select('aws_index_data', 'a');
    $query->fields('a', [
      'id',
      'index_id',
      'entity',
      'bundles',
      'created_at',
      'updated_at',
    ]);
    if (!empty($args['id'])) {
      $query->condition('a.id', $args['id']);
    }
    if (!empty($args['index_id'])) {
      $query->condition('a.index_id', $args['index_id']);
    }
    if (!empty($args['entity'])) {
      $query->condition('a.entity', $args['entity']);
    }
    // To fetch single record.
    if (!empty($args['id'])) {
      $result = $query->execute()->fetch();
    }
    else {
      // To fetch multiple records.
      $result = $query->execute()->fetchAll();
    }
    return $result;
  }

  /**
   * Fetch values from objects.
   *
   * @param object $indexDataResult
   *    Result object.
   * @param string $key
   *    Key value.
   *
   * @return array
   *    Return array of values.
   */
  public function getValueByKey($indexDataResult, $key) {
    return array_column(\GuzzleHttp\json_decode(\GuzzleHttp\json_encode($indexDataResult), TRUE), $key);
  }

  /**
   * Fetch entity's fields.
   *
   * @param string $entity_id
   *    Entity id.
   * @param string $bundle
   *    Entity bundle.
   *
   * @return array
   *    Return bundle's fields.
   */
  public function getEntityFields($entity_id, $bundle) {
    $fieldDefinitions = $this->entityFieldManager->getFieldDefinitions($entity_id, $bundle);
    foreach ($fieldDefinitions as $field_name => $field_definition) {
      $label = $field_definition->getLabel();
      $type = $field_definition->getType();
      if (method_exists($field_definition->getLabel(), '__toString')) {
        $label = $field_definition->getLabel()->__toString();
      }
      //echo "<pre>"; print_r($field_name); echo "</pre>";
      if (!empty($field_definition->getTargetBundle()) || !in_array($field_name, $this->notAllowedFields)) {
        $bundleFields[$entity_id][$field_name]['type'] = $type;
        $bundleFields[$entity_id][$field_name]['label'] = $label;
      }
    }
    return $bundleFields;
  }

}
