<?php

namespace Drupal\aws_cloudsearch\Helper;

use Aws\CloudSearchDomain\CloudSearchDomainClient;
use Drupal\Core\Url;
use \GuzzleHttp\json_encode;
use \GuzzleHttp\json_decode;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class AwsApi {

  const AWS_ACCESS_KEY = 'AKIAJYZTN2XICWBCPRAA';
  const AWS_SECRET_KEY = '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';
  const AWS_REGION = 'ap-southeast-1';
  // Wordpress Blog id in AWS cloudsearch.
  const AWS_WP_BLOG_ID = 1;
  // Drupal Blog id in AWS cloudsearch.
  const AWS_DRUPAL_BLOG_ID = 2;
  // Drupal content default priority.
  const DRUPAL_PRIORITY = 2;
  // Site id in AWS cloudsearch.
  const AWS_SITE_ID = 1;
  // AWS queryParser value.
  const AWS_QUERY_PARSER = 'lucene';
  // Content URL in search.
  const SITE_URL = 'http://www.tothenew.com';

  public $awsClint = NULL;
  public $limit = 20;
  public $offset = 0;
  public $request;
  // View URL from where read data to ingest.
  private $view_url;
  private $aws_domain;
  private $base_url = NULL;
  public static $awsRegions;

  /**
   * Keep class object
   * @var object
   */
  public static $_awsapi = NULL;

  /**
   * Get class instance using this function.
   * @return AwsAPI
   */
  public static function getInstance() {
    //echo "/vendor/autoload.php";
    if (!self::$_awsapi) {
      self::$_awsapi = new AwsApi();
    }
    self::$_awsapi->init();
    return self::$_awsapi;
  }

  /**
   *
   * @return string
   *    Return AWS Endpoint.
   */
  public function getAwsEndpoint() {
    return 'search-' . $this->aws_domain . '.' . self::AWS_REGION . '.cloudsearch.amazonaws.com';
  }

  /**
   *
   * @return object
   *    Return CloudSearchDomainClient object.
   */
  public function init() {
    $this->request = \Drupal::service('request_stack');
    $this->setAwsConfig();
    try {
      $this->awsClint = CloudSearchDomainClient::factory(array(
            'endpoint' => $this->getAwsEndpoint(),
            'region' => self::AWS_REGION,
            'credentials' => array(
              'key' => self::AWS_ACCESS_KEY,
              'secret' => self::AWS_SECRET_KEY
            )
      ));
      return $this->awsClint;
    }
    catch (\Exception $e) {
      //echo "<pre>Error:"; print_r($e->getMessage()); echo "</pre>";
      exit();
    }
  }
  

  public function setAwsConfig() {
    // AWS Live credentials.
    $host = $this->request->getCurrentRequest()->getHost();
    // AWS Local credentials.
    if ($host == 'dreview.com') {
      $this->view_url = 'http://dreview.com';
      $this->aws_domain = 'dreview-jz7puzlm36xpbbp3ko46ncp734';
      $this->base_url = '/ttndrupal';
    }
    elseif ($host == 'tothenew.com' || $host == 'www.tothenew.com') {
      // AWS Live credentials.
      $this->view_url = 'http://www.tothenew.com';
      $this->aws_domain = 'ttnlive-m6ca6czmdxa2hiwolyozz6lvhu';
    } else {
      // AWS Dev credentails.
      $this->view_url = 'http://'.$host;
      $this->aws_domain = 'ttndev-im2ypw33fv6eagcgmqb4cp5qka';
    }
  }

  /**
   * Put to index a list of documents
   *
   * @param array $docs
   *
   * @return bool
   */
  function uploadAwsDocuments($docs) {
    try {
      // Upload documents.
      $result = $this->awsClint->uploadDocuments(array(
        'documents' => json_encode($docs),
        'contentType' => 'application/json'
      ));
      // Return result
      return $result->getPath('status') == 'success';
    }
    catch (\Exception $e) {
      drupal_set_message(t('Error uploading documents to AWS cloudsearch'), 'error');
      return false;
    }
  }

  public function searchAwsDocuments($query) {
    $sort = ' blog_id desc, _score desc';
    // Prepare search args
    $query = $this->getQuery($query);
    $start = $this->getResultOffset();
    $limit = $this->getResultLimit();
    $search_args = array(
      'query' => $query,
      'start' => $start,
      'size' => $limit,
      'filterQuery' => $this->getFilterQuery(),
      'queryParser' => self::AWS_QUERY_PARSER,
      'sort' => $sort
    );
    //echo "<pre>search_args:"; print_r($search_args); echo "</pre>";
    if (!empty($facets))
      $search_args['facet'] = json_encode($facets);
    // Read result
    $result = $this->awsClint->search($search_args);
    $hit_found = $result->getPath('hits/found'); // Total items
    $items = $result->getPath('hits'); // Items array
    $facets_result = $result->getPath('facets'); // Facets
    $resultData = ['status' => $result->getPath('status'),
      'offset' => $this->getResultOffset(),
      'limit' => $this->getResultLimit(),
      'total' => $hit_found,
      'items' => $items,
      'facets' => $facets_result
    ];
    return $resultData;
  }

  /**
   *
   * @return array
   *    Return Drupal and wordpress blog, site id where AWS will search.
   */
  public function getFilterQuery() {
    return '(and (and site_id:' . self::AWS_SITE_ID . ')) ';
  }

  /**
   *
   * @param string $query
   *    Query to search in AWS cloudsearch.
   *
   * @return string
   *    Return string.
   */
  public function getQuery($query) {
    return (empty($query)) ? '*' : $query;
  }

  /**
   * Get Result limit value.
   *
   * @return int
   *    REturn result limit value.
   */
  public function getResultLimit() {
    return $this->limit;
  }

  /**
   * Set result limit value.
   *
   * @param int $limit
   *    Return limit value.
   */
  public function setResultLimit($limit) {
    $this->limit = intval($limit);
  }

  /**
   * Get result offset value.
   *
   * @return int
   *    Return result offset value.
   */
  public function getResultOffset() {
    return $this->offset;
  }

  /**
   * Set result offset value to search.
   *
   * @param int $offset
   *    Offset value.
   */
  public function setResultOffset($offset) {
    $this->offset = intval($offset);
  }

  /**
   * Ingest content to AWS cloudsearch.
   *
   * @param int $nid
   *    Node id to delete single content .
   * @param string $action
   *    Action to perform [add, update, delete].
   * @param int $page
   *    Page number to perform bulk operation.
   */
  public function ingestAwsContent($nid, $action, $page = 1, $node = NULL) {
    $site_id = self::AWS_SITE_ID;
    $blog_id = self::AWS_DRUPAL_BLOG_ID;
    $options = array('absolute' => FALSE);
    // If node is deleted.
    if ($action == 'delete' && (!empty($nid))) {
      $search_id = $site_id . '_' . $blog_id . '_' . $nid;
      $docs[] = ['type' => 'delete', 'id' => $search_id];
    }
    elseif ($action == 'add') {
      // If node add or update on Entity update/add.
      if (!empty($node)) {
        $json = json_encode($node->toArray());
        $jsonData[] = json_decode($json);
      }
      else {
        // For the bulk operation.
        $json = file_get_contents($this->view_url . $this->base_url . '/post_aws_cloudsearch_index?page=' . $page);
        $jsonData = json_decode($json);
      }
      
      // For the node add, update.
      foreach ($jsonData as $json) {
        $id = $json->nid[0]->value;
        $title = $json->title[0]->value;
        $status = $json->status[0]->value;
        $created = $json->created[0]->value;
        $body = (isset($json->body[0])) ? $json->body[0]->value : $title;
        $post_type = $json->type[0]->target_id;
        $metaTag = (isset($json->field_meta_tag[0])) ? $json->field_meta_tag[0]->value : NULL;
        // Assign meta title to Title if exists.
        // Assign description into metaDescription.
        // Assign keywords, abstract and title into post_extra.
        $metaTags = $this->getMetaTags($metaTag, $title);
        $url = Url::fromRoute('entity.node.canonical', ['node' => $id], $options)->toString();
        // Ingest content that have SEO friendly URL.
        if (strstr($url, '/node/') == FALSE) {
          $fields = [];
          // AWS cloudsearch ID.
          $search_id = $site_id . '_' . $blog_id . '_' . $id;

          $title = $this->filterTitle($metaTags['title']);
          $fields['id'] = $id;
          $fields['post_title'] = $title;
          $fields['post_content'] = $body;
          if (!empty($metaTags['description']))
          $fields['post_excerpt'] = $metaTags['description'];
          if (!empty($metaTags['post_extra']))
          $fields['post_extra'] = $metaTags['post_extra'];
          $fields['post_url'] = $this->getSiteUrl($url);
          $fields['site_id'] = $site_id;
          $fields['blog_id'] = self::AWS_DRUPAL_BLOG_ID;
          $fields['cf_priority'] = $this->getContentPriority($post_type);
          $fields['blog_id'] = $blog_id;
          $fields['post_date'] = $created;
          $fields['post_type'] = $post_type;
          $type = ($status == 1) ? 'add' : 'delete';
          $docs[] = ['type' => $type, 'id' => $search_id, 'fields' => $fields];
        }
      }
    }

    if (!empty($docs)) {
      $result = $this->awsClint->uploadDocuments(array(
        'documents' => json_encode($docs),
        'contentType' => 'application/json',
      ));
      return $result;
    }
    return TRUE;
  }

  /**
   * Get content priority based on Content type.
   *
   * @param string $post_type
   *    Content type.
   *
   * @return int
   *    Return priority value default 2.
   */
  public function getContentPriority($post_type) {
    $contentType['services'] = 3;
    $contentType['article'] = 3;
    $contentType['landing_pages'] = 3;
    $contentType['newsroom_and_media'] = 1;
    return (isset($contentType[$post_type])) ? $contentType[$post_type] : self::DRUPAL_PRIORITY;
  }

  /**
   * Get content URL that to ingest to AWS cloudsearch.
   *
   * @param string $url
   *    Content URL.
   *
   * @return string
   *    URL.
   */
  public function getSiteUrl($url) {
    return str_replace($this->base_url, '', self::SITE_URL . $url);
  }

  /**
   * Get meta tags's title, description and post_extra.
   *
   * @param object $metaTags
   *    Metatags serialiaze data.
   *
   * @param string $title
   *    Page title.
   *
   * @return array
   *    Return array of title, description and post_extra items.
   *    
   */
  public function getMetaTags($metaTags, $title) {
    $return['title'] = NULL;
    $return['description'] = NULL;
    $return['post_extra'] = NULL;
    if (!empty($metaTags)) {
      $metaTags = unserialize($metaTags);
      // Assign meta title to title if not empty.
      $return['title'] = !empty($metaTags['title']) ? $metaTags['title']: $title;
      $return['description'] = !empty($metaTags['description']) ? $metaTags['description']: NULL;
      // Concat keywords, abstract and title.
      $keyword = !empty($metaTags['keywords']) ? $metaTags['keywords']: NULL;
      $abstract = !empty($metaTags['abstract']) ? $metaTags['abstract']: NULL;
      
      if (!empty($title))
      $extra[] = $title;
      if (!empty($keyword))
        $extra[] = $keyword;
      if (!empty($abstract))
        $extra[] = $abstract;
      
      if (count($extra) > 0)
      $return['post_extra'] = implode(', ', $extra);
      
    }
    return $return;
  }

  /**
   * Remove substring after |. Remove To The New from meta title.
   *
   * @param string $title
   *    Title string.
   *
   * @return string
   *    Return title string.
   */
  public function filterTitle($title) {
    $titleArr = explode('|', $title);
    return (!empty($titleArr[0])) ? trim($titleArr[0]) : $title;
  }

  /**
   * AWS predefined regions.
   */
  public static function getAwsRegions() {
    self::$awsRegions['us-east-1'] = 'US East (N. Virginia)';
    self::$awsRegions['us-east-2'] = 'US East (Ohio)';
    self::$awsRegions['us-west-1'] = 'US West (N. California)';
    self::$awsRegions['us-west-2'] = 'US West (Oregon)';
    self::$awsRegions['ca-central-1'] = 'Canada (Central)';
    self::$awsRegions['eu-central-1'] = 'EU (Frankfurt)';
    self::$awsRegions['eu-west-1'] = 'EU (Ireland)';
    self::$awsRegions['eu-west-2'] = 'EU (London)';
    self::$awsRegions['ap-southeast-1'] = 'Asia Pacific (Singapore)';
    self::$awsRegions['ap-southeast-2'] = 'Asia Pacific (Sydney)';
    self::$awsRegions['ap-northeast-1'] = 'Asia Pacific (Tokyo)';
    self::$awsRegions['ap-northeast-2'] = 'Asia Pacific (Seoul)';
    self::$awsRegions['ap-south-1'] = 'Asia Pacific (Mumbai)';
    self::$awsRegions['sa-east-1'] = 'South America (Sao Paulo)';
    return self::$awsRegions;
  }

}
