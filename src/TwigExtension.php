<?php

namespace Drupal\aws_cloudsearch;

/**
 * Class TwigExtension
 */
class TwigExtension extends \Twig_Extension {

  /**
   * Protected container variable.
   *
   * @var container
   */
  protected $container;

  /**
   * Twig functions listener.
   *
   * @return array
   *    Return array.
   */
  public function getFunctions() {
    return array(new \Twig_SimpleFunction('add_userpath_class', array($this, 'addUserPagesClass'))
    );
  }

  /**
   * Add path-user class in body.
   */
  public function addUserPagesClass() {
    $this->container = \Drupal::getContainer();
    $request = $this->container->get('request_stack');
    $request = $request->getCurrentRequest();
    $response = $this->getUserPages($request->getPathInfo());
    if ($response == TRUE) {
      return 'user-path';
    }
    else {
      return 'user-path-no';
    }
  }

  /**
   * Check current URI is user pages or not.
   *
   * @param string $requestUri
   *    Current request URI.
   *
   * @return bool
   *    Return true|false
   */
  public function getUserPages($requestUri) {
    $pages = \Drupal::service('custom.configuration')->getValue('user_pages');
    $isAllow = FALSE;
    foreach (preg_split("/((\r?\n)|(\r\n?))/", $pages) as $line) {
      if (!empty($line)) {
        $pattern = addcslashes($line, '/');
        if (preg_match("/$pattern/", $requestUri)) {
          $isAllow = TRUE;
          break;
        }
      }
    }
    return $isAllow;
  }

}
