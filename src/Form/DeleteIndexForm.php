<?php

namespace Drupal\aws_cloudsearch\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\aws_cloudsearch\Helper\AwsHelper;
use Drupal\Core\StringTranslation\TranslationManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Class Delete Index.
 *
 * @package Drupal\aws_cloudsearch\Form
 */
class DeleteIndexForm extends ConfigFormBase {

  /**
   * Drupal\Core\StringTranslation\TranslationManager definition.
   *
   * @var \Drupal\Core\StringTranslation\TranslationManager
   */
  protected $stringTranslation;
  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new DefaultForm object.
   */
  public function __construct(
    TranslationManager $string_translation,
    RequestStack $request_stack
  ) {
    $this->stringTranslation = $string_translation;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'aws_cloudsearch.index_config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('string_translation'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'delete_index_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $awsHelper    = AwsHelper::getInstance();
    $request = $this->requestStack->getCurrentRequest();
    $id = $request->attributes->get('id');
    $result = $awsHelper->getIndexes(['id' => $id]);
    if ($result) {
      $form['helptext'] = [
        '#type' => 'item',
        '#markup' => "Are you sure you want to delete the <strong>" . $result->name . "</strong> Index ?",
      ];
      $form['delete'] = [
        '#type' => 'submit',
        '#value' => $this->t('Delete'),
      ];
    }
    else {
      drupal_set_message($this->t('Index does not exists.'), 'error');
    }
    $link = Link::createFromRoute(
        $this->t('<strong>Cancel</strong>'), 'aws_cloudsearch.index')->toString();
    unset($form['actions']);
    $form['edit'] = [
      '#markup' => $link,
    ];
    return $form;
  }

  /**
   * Submission of the delete form.
   *
   * @param array $form
   *   Form information.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   Form value.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $awsHelper    = AwsHelper::getInstance();
    $request = $this->requestStack->getCurrentRequest();
    $id = $request->attributes->get('id');
    $result = $awsHelper->getIndexes(['id' => $id]);
    if (isset($result)) {
      $awsHelper->deleteIndex($id);
      drupal_set_message($this->t("Index <strong>@name</strong> has been deleted successfully.", ['@name' => $result->name]), 'status');
    }
    $form_state->setRedirectUrl(Url::fromRoute('aws_cloudsearch.index'));
  }

}
