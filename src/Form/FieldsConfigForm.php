<?php

namespace Drupal\aws_cloudsearch\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\TranslationManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\aws_cloudsearch\Helper\AwsHelper;
use Drupal\Component\Utility\Html;

/**
 * Class FieldsConfigForm.
 */
class FieldsConfigForm extends FormBase {

  /**
   * Drupal\Core\StringTranslation\TranslationManager definition.
   *
   * @var \Drupal\Core\StringTranslation\TranslationManager
   */
  protected $stringTranslation;
  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;
  /**
   * Constructs a new DefaultForm object.
   */

  public function __construct(
    TranslationManager $string_translation,
    RequestStack $request_stack
  ) {
    $this->stringTranslation = $string_translation;
    $this->requestStack = $request_stack;
  }

  /**
   * Manage dependency injection.
   *
   * @param ContainerInterface $container
   *    Container object.
   *
   * @return \static
   *    Return objects.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('string_translation'),
      $container->get('request_stack')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fields_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $awsHelper = AwsHelper::getInstance();
    // Set an appropriate page title.
    $form['#title'] = $this->t('Manage fields for search index');
    $form['#tree'] = TRUE;
    $indexData = $awsHelper->getIndexData();
    foreach ($indexData as $key => $index) {
      $bundleFields = [];
      $entity = $index->entity;
      $bundles = unserialize($index->bundles);
      foreach ($bundles as $bundle) {
        $fields = $awsHelper->getEntityFields($entity, $bundle);
        $bundleFields = array_merge($bundleFields, $fields[$entity]);
      }
      $form['fields'][$key] = $this->buildFieldsTable($bundleFields, $entity);
      $form['fields'][$key]['#title'] = $awsHelper->getEntityLabel($entity);
    }
    $form['fields']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  public function buildFieldsTable($bundleFields, $entity) {
    //echo "<pre>"; print_r($bundleFields); echo "</pre>"; die;
    $build = [
      '#type' => 'details',
      '#open' => TRUE,
      '#theme' => 'search_api_admin_fields_table',
      '#parents' => [],
      '#header' => [
        $this->t('Select Field'),
        $this->t('Machine name'),
//        $this->t('Label'),
//        [
//          'data' => $this->t('Description'),
//          'class' => [RESPONSIVE_PRIORITY_LOW],
//        ],
//        $this->t('Type'),
      ],
    ];
    foreach ($bundleFields as $key => $fileds) {
      $build['fields'][$key]['id'] = [
        '#type' => 'checkbox',
        '#default_value' => $key,
        //'#required' => TRUE,
        '#size' => 40,
      ];
      $build['fields'][$key]['machine_name'] = [
        '#markup' => Html::escape($key),
      ];
//      $build['fields'][$entity][$key]['title'] = [
//        '#markup' => Html::escape($fileds['label']),
//      ];
//      $build['fields'][$entity][$key]['description'] = [
//        '#markup' => Html::escape('description'),
//      ];
//      $build['fields'][$entity][$key]['type'] = [
//        '#markup' => Html::escape($fileds['type']),
//      ];
    }
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    $fields = $form_state->getValue('fields');
    unset($fields['submit']);
    echo "<pre>"; print_r($fields); echo "</pre>";
    //kint($fields);
    foreach ($fields as $key => $value) {
      //drupal_set_message($key . ': ' . $value);
      //echo "<pre>$key : "; print_r($value); echo "</pre>";
    }
    die;

  }

}
