<?php

namespace Drupal\aws_cloudsearch\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\TranslationManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\aws_cloudsearch\Helper\AwsHelper;
use Drupal\Core\Link;

/**
 * Class DomainForm.
 */
class DomainForm extends ConfigFormBase {

  /**
   * Drupal\Core\StringTranslation\TranslationManager definition.
   *
   * @var \Drupal\Core\StringTranslation\TranslationManager
   */
  protected $stringTranslation;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new DefaultForm object.
   */
  public function __construct(
  TranslationManager $string_translation, RequestStack $request_stack
  ) {
    $this->stringTranslation = $string_translation;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'aws_cloudsearch.domain_config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('string_translation'), $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'domain_list_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $add_link = Link::createFromRoute($this->t('Add Domain'), 'aws_cloudsearch.domain_add')->toString();
    $awsHelper = AwsHelper::getInstance();
    $results = $awsHelper->getDomains();
    $form['config-list'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('AWS Domain & Region'),
        $this->t('Status'),
        $this->t('Updated'),
        $this->t('actions'),
      ],
      '#empty' => $this->t('There are no items yet @add_link.', ['@add_link' => $add_link]),
    ];
    if ($results != FALSE) {
      foreach ($results as $result) {
        $result->updated_at = date('d M,Y H:i:s', $result->updated_at);
        $form['config-list'][$result->id]['name'] = [
          '#markup' => $result->name,
        ];
        $form['config-list'][$result->id]['status'] = [
          '#markup' => ($result->status == 1) ? 'Active' : 'Inactive',
        ];
        $form['config-list'][$result->id]['updated'] = [
          '#markup' => $result->updated_at,
        ];
        $edit_link = Link::createFromRoute(
                $this->t('Edit'), 'aws_cloudsearch.domain_edit', ['id' => $result->id]
            )->toString();
        $delete_link = Link::createFromRoute($this->t('Delete'), 'aws_cloudsearch.domain_delete', ['id' => $result->id])->toString();
        $form['config-list'][$result->id]['actions'] = [
          '#markup' => $edit_link . ' | ' . $delete_link,
        ];
      }
    }
    return $form;
  }

}
