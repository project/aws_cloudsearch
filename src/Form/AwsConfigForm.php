<?php

namespace Drupal\aws_cloudsearch\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\TranslationManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\aws_cloudsearch\Helper\AwsApi;

/**
 * Class AwsConfigForm.
 */
class AwsConfigForm extends ConfigFormBase {

  /**
   * Drupal\Core\StringTranslation\TranslationManager definition.
   *
   * @var \Drupal\Core\StringTranslation\TranslationManager
   */
  protected $stringTranslation;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
      TranslationManager $string_translation
    ) {
    parent::__construct($config_factory);
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('string_translation')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'aws_cloudsearch.awsconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aws_config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $regionsList = AwsApi::getAwsRegions();
    $config = $this->config('aws_cloudsearch.awsconfig');
    $form['aws_access_key'] = [
      '#type' => 'textfield',
      '#maxlength' => 50,
      '#required' => TRUE,
      '#title' => $this->t('AWS Access Key'),
      '#default_value' => $config->get('aws_access_key'),
    ];
    $form['aws_secret_key'] = [
      '#type' => 'textfield',
      '#maxlength' => 100,
      '#required' => TRUE,
      '#title' => $this->t('AWS Secret Key'),
      '#description' => $this->t('AWS Secret Key'),
      '#default_value' => $config->get('aws_secret_key'),
    ];
    $form['aws_region_name'] = [
      '#type' => 'select',
      '#options' => $regionsList,
      '#required' => TRUE,
      '#title' => $this->t('AWS Region'),
      '#description' => $this->t('Enter AWS Region name'),
      '#default_value' => $config->get('aws_region_name'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('aws_cloudsearch.awsconfig')
      ->set('aws_access_key', $form_state->getValue('aws_access_key'))
      ->set('aws_secret_key', $form_state->getValue('aws_secret_key'))
      ->set('aws_region_name', $form_state->getValue('aws_region_name'))
      ->save();
  }

}
