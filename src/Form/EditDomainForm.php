<?php

namespace Drupal\aws_cloudsearch\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\TranslationManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\aws_cloudsearch\Helper\AwsHelper;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\aws_cloudsearch\Controller\DomainController;

/**
 * Class EditDomainForm.
 */
class EditDomainForm extends ConfigFormBase {

  /**
   * Drupal\Core\StringTranslation\TranslationManager definition.
   *
   * @var \Drupal\Core\StringTranslation\TranslationManager
   */
  protected $stringTranslation;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new DefaultForm object.
   */
  public function __construct(
  TranslationManager $string_translation, RequestStack $request_stack
  ) {
    $this->stringTranslation = $string_translation;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'aws_cloudsearch.domain_config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('string_translation'), $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'domain_edit_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $request = $this->requestStack->getCurrentRequest();
    $id = $request->attributes->get('id');
    $awsHelper = AwsHelper::getInstance();
    // Fetch the domain result by ID.
    $result = $awsHelper->getDomains(['id' => $id]);
    $form = parent::buildForm($form, $form_state);
    if (!empty($result)) {
      $form['domain']['name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('AWS Domain & Region'),
        '#required' => TRUE,
        '#maxlength' => 150,
        '#placeholder' => 'prod-jz7puzlm36xpbbp3ko46ncp734.ap-southeast-1',
        '#default_value' => $result->name,
        '#description' => '<small>' . $this->t('Enter domain name with Region. For Ex: prod-jz7puzlm36xpbbp3ko46ncp734.ap-southeast-1.'),
      ];
      $form['domain']['status'] = [
        '#type' => 'select',
        '#title' => $this->t('Status'),
        '#options' => [
          '0' => $this->t('Inactive'),
          '1' => $this->t('Active'),
        ],
        '#default_value' => $result->status,
        '#description' => '<small>' . $this->t('if the domain is inactive no content will be accessed from/to AWS cloud search') . '</small>',
      ];
      $form['domain']['id'] = [
        '#type' => 'hidden',
        '#required' => TRUE,
        '#default_value' => $result->id,
      ];
      $form['submit'] = [
        '#type' => 'submit',
        '#weight' => 10,
        '#value' => $this->t('Update'),
        '#attributes' => ['class' => ['button button--primary js-form-submit form-submit']],
      ];
    }
    else {
      drupal_set_message('Domain does not exists.', 'error');
    }
    $link = Link::createFromRoute(
            $this->t('<strong>Cancel</strong>'), 'aws_cloudsearch.domain')->toString();
    unset($form['actions']);
    $form['edit'] = [
      '#markup' => $link,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $awsHelper = AwsHelper::getInstance();
    $name = $form_state->getValue('name');
    $id = $form_state->getValue('id');
    // Check domain already exists or not.
    if ($awsHelper->getDomains(['name' => $name, 'not_id' => $id]) != FALSE) {
      $form_state->setErrorByName('name', $this->t('Domain name <strong>@name</strong> already exists.', ['@name' => $name]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $awsHelper = AwsHelper::getInstance();
    $id = $form_state->getValue('id');
    $name = $form_state->getValue('name');
    $status = $form_state->getValue('status');
    // Update domain details.
    $result = $awsHelper->updateDomain(['name' => $name, 'status' => $status], $id);
    // On success set message and redirect the Form.
    if ($result['status'] == 'success') {
      drupal_set_message($result['message'], 'status');
      $form_state->setRedirectUrl(Url::fromRoute('aws_cloudsearch.domain'));
    }
    else {
      drupal_set_message($result['message'], 'error');
    }
  }

}
