<?php

namespace Drupal\aws_cloudsearch\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\TranslationManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\aws_cloudsearch\Helper\AwsHelper;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class AddDomainForm.
 */
class AddDomainForm extends ConfigFormBase {

  /**
   * Drupal\Core\StringTranslation\TranslationManager definition.
   *
   * @var \Drupal\Core\StringTranslation\TranslationManager
   */
  protected $stringTranslation;
  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new DefaultForm object.
   */
  public function __construct(
    TranslationManager $string_translation,
    RequestStack $request_stack
  ) {
    $this->stringTranslation = $string_translation;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'aws_cloudsearch.domain_config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('string_translation'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'domain_add_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $awsHelper    = AwsHelper::getInstance();
    // If there is already a domain added, redirect on the list page.
    $result = $awsHelper->getDomains();
    if ($result) {
      $url = Url::fromRoute('aws_cloudsearch.domain')->toString();
      return new RedirectResponse($url);
    }
    $config     = $this->config('aws_cloudsearch.domain_config');
    $form = parent::buildForm($form, $form_state);
    $form['domain']['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('AWS Domain & Region'),
      '#required' => TRUE,
      '#maxlength' => 150,
      '#placeholder' => 'prod-jz7puzlm36xpbbp3ko46ncp734.ap-southeast-1',
      '#default_value' => $config->get('name'),
      '#description' => '<small>' . $this->t('Enter domain name with Region. For Ex: prod-jz7puzlm36xpbbp3ko46ncp734.ap-southeast-1.') . '</small>',
    ];
    $link = Link::createFromRoute(
      $this->t('<br><strong>Cancel</strong>'), 'aws_cloudsearch.domain')->toString();
    unset($form['actions']);
    $form['submit'] = [
      '#type' => 'submit',
      '#weight' => 10,
      '#value' => $this->t('Submit'),
      '#attributes' => ['class' => ['button button--primary js-form-submit form-submit']],
    ];
    $form['edit'] = [
      '#markup' => $link,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $awsHelper    = AwsHelper::getInstance();
    $name         = $form_state->getValue('name');
    // Check domain is exists or not.
    if ($awsHelper->getDomains(['name' => $name]) != FALSE) {
      $form_state->setErrorByName('name', $this->t('Domain name <strong>@name</strong> already exists.', ['@name' => $name]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $awsHelper = AwsHelper::getInstance();
    $name = $form_state->getValue('name');
    // Save domain.
    $result = $awsHelper->saveDomain(['name' => $name]);
    // On success set success message and redirect Form.
    if ($result['status'] == 'success') {
      drupal_set_message($result['message'], 'status');
      $form_state->setRedirectUrl(Url::fromRoute('aws_cloudsearch.domain'));
    }
    else {
      drupal_set_message($result['message'], 'error');
    }
  }

}
