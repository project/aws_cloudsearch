<?php

namespace Drupal\aws_cloudsearch\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\TranslationManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\aws_cloudsearch\Helper\AwsHelper;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\aws_cloudsearch\Controller\DomainController;

/**
 * Class EditIndexForm.
 */
class EditIndexForm extends ConfigFormBase {

  /**
   * Drupal\Core\StringTranslation\TranslationManager definition.
   *
   * @var \Drupal\Core\StringTranslation\TranslationManager
   */
  protected $stringTranslation;
  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new DefaultForm object.
   */
  public function __construct(
    TranslationManager $string_translation,
    RequestStack $request_stack
  ) {
    $this->stringTranslation = $string_translation;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'aws_cloudsearch.index_config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('string_translation'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'index_edit_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $awsHelper = AwsHelper::getInstance();
    $request = $this->requestStack->getCurrentRequest();
    $id = $request->attributes->get('id');
    // Entitties list.
    $dataSource = $awsHelper->getDataSource();
    $domain = new DomainController();
    // Get all domains.
    $domainsList = $awsHelper->getDomains();
    $domainsList = $domain->converToArray($domainsList);
    $form = parent::buildForm($form, $form_state);
    $result = $awsHelper->getIndexes(['id' => $id]);
    if (!empty($result)) {
      $indexDataResult = $awsHelper->getIndexData(['index_id' => $result->id]);
      $entities = $awsHelper->getValueByKey($indexDataResult, 'entity');
      $form['index']['domain_id'] = [
        '#type' => 'select',
        '#title' => $this->t('AWS Domain & Region'),
        '#required' => TRUE,
        '#options' => $domainsList,
        '#default_value' => $result->domain_id,
      ];
      $form['index']['name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Index Name'),
        '#required' => TRUE,
        '#maxlength' => 150,
        '#placeholder' => 'prod-jz7puzlm36xpbbp3ko46ncp734',
        '#default_value' => $result->name,
        '#description' => '<small>' . $this->t('Index name must have alphanumeric only.') . '</small>',
      ];
      $form['index']['entity'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Data Source'),
        '#options' => $dataSource,
        '#required' => TRUE,
        '#default_value' => $entities,
        '#ajax' => [
          'callback' => '::fetchDataSource',
          'effect' => 'fade',
          'wrapper' => 'datasources_wrapper',
          'event' => 'change',
          'progress' => [
            'type' => 'throbber',
            'message' => NULL,
          ],
        ],
      ];
      $form['index']['entity_container'] = [
        '#type' => 'container',
        '#attributes' => ['id' => 'datasources_wrapper'],
        '#tree' => TRUE,
        '#optional' => TRUE,
      ];
      if ($form_state->getValue('entity', NULL) or ($entities)) {
        $sources = ($form_state->getValue('entity')) ? $form_state->getValue('entity') : $entities;
        foreach ($sources as $key => $value) {
          if (!empty($value)) {
            $nodeBundles = $awsHelper->getBundles($value);
            $indexDataResult = $awsHelper->getIndexData(['index_id' => $result->id, 'entity' => $value]);
            $bundles = $awsHelper->getValueByKey($indexDataResult, 'bundles');
            $form['index']['entity_container'][$value] = [
              '#type' => 'checkboxes',
              '#prefix' => '<div class="entity_bundles accordion-toggle ' . $value . '">',
              '#suffix' => '</div>',
              '#title' => $this->t('Configure the %datasource datasource', ['%datasource' => $awsHelper->getEntityLabel($value)]),
              '#options' => $nodeBundles,
              '#required' => TRUE,
              '#default_value' => unserialize($bundles[0]),
            ];
          }
        }
      }
      $form['index']['id'] = [
        '#type' => 'hidden',
        '#required' => TRUE,
        '#maxlength' => 10,
        '#default_value' => $result->id,
      ];
      $form['submit'] = [
        '#type' => 'submit',
        '#weight' => 10,
        '#value' => $this->t('Update'),
        '#attributes' => ['class' => ['button button--primary js-form-submit form-submit']],
      ];
    }
    else {
      drupal_set_message('Index does not exists.', 'error');
    }
    $link = Link::createFromRoute(
        $this->t('<strong>Cancel</strong>'), 'aws_cloudsearch.index')->toString();
    unset($form['actions']);
    $form['edit'] = [
      '#markup' => $link,
    ];
    return $form;
  }

  /**
   * Handles changes to the selected datasources.
   *
   * @param array $form
   *   The current form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return array
   *   The part of the form to return as AJAX.
   */
  public function fetchDataSource(array $form, FormStateInterface $form_state) {
    return $form['index']['entity_container'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $awsHelper = AwsHelper::getInstance();
    $name = $form_state->getValue('name');
    $id = $form_state->getValue('id');
    // Check index name is exists or not.
    if ($awsHelper->getIndexes(['name' => $name, 'not_id' => $id]) != FALSE) {
      $form_state->setErrorByName('name', $this->t('Index name <strong>@name</strong> already exists.', ['@name' => $name]));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $awsHelper  = AwsHelper::getInstance();
    $id         = $form_state->getValue('id');
    $name       = $form_state->getValue('name');
    $domain_id  = $form_state->getValue('domain_id');
    $entity_container = $form_state->getValue('entity_container');
    // Remove items that have value as 0.
    $entity_container = $awsHelper->getSelectedTypes($entity_container);
    $data = [];
    $data['name'] = $name;
    $data['domain_id'] = $domain_id;
    $data['entities'] = $entity_container;
    // Save domain.
    $result = $awsHelper->updateIndex($data, $id);
    // On success set success message and redirect Form.
    if ($result['status'] == 'success') {
      drupal_get_messages('status');
      drupal_set_message($result['message'], 'status');
      $form_state->setRedirectUrl(Url::fromRoute('aws_cloudsearch.index'));
    }
    else {
      drupal_set_message($result['message'], 'error');
    }
  }

}
