<?php

namespace Drupal\aws_cloudsearch\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\TranslationManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\aws_cloudsearch\Helper\AwsHelper;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\aws_cloudsearch\Controller\DomainController;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Entity\EntityManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\search_api\Utility\PluginHelperInterface;

/**
 * Class AddIndexForm.
 */
class AddIndexForm extends ConfigFormBase {

  /**
   * Drupal\Core\StringTranslation\TranslationManager definition.
   *
   * @var \Drupal\Core\StringTranslation\TranslationManager
   */
  protected $stringTranslation;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;
  protected $entityManager;
  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new AwsConfig object.
   */
  public function __construct(
      ConfigFactoryInterface $config_factory,
      TranslationManager $string_translation,
      RequestStack $request_stack,
      EntityManager $entityManager,
      EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($config_factory);
    $this->stringTranslation = $string_translation;
    $this->requestStack = $request_stack;
    $this->entityManager = $entityManager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('config.factory'), $container->get('string_translation'), $container->get('request_stack'), $container->get('entity.manager'), $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'aws_cloudsearch.index_config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aws_index_config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $awsHelper = AwsHelper::getInstance();
    $config = $this->config('aws_cloudsearch.index_config');
    $domain = new DomainController();
    $domainsList = $awsHelper->getDomains();
    $domainsList = $domain->converToArray($domainsList);
    $dataSource = $awsHelper->getDataSource();
    echo "<pre>"; print_r($dataSource); echo "</pre>";
    $form['index']['domain_id'] = [
      '#type' => 'select',
      '#title' => $this->t('AWS Domain & Region'),
      '#required' => TRUE,
      '#options' => $domainsList,
      '#default_value' => $config->get('domain'),
    ];
    $form['index']['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Index Name'),
      '#required' => TRUE,
      '#maxlength' => 30,
      '#default_value' => $config->get('name'),
    ];
    $form['index']['entity'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Data Source'),
      '#options' => $dataSource,
      '#required' => TRUE,
      '#default_value' => $config->get('entity'),
      '#ajax' => [
        'callback' => '::fetchDataSource',
        'effect' => 'fade',
        'wrapper' => 'datasources_wrapper',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
    ];
    $form['index']['entity_container'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'datasources_wrapper'],
      '#tree' => TRUE,
      '#optional' => TRUE,
    ];
    if ($form_state->getValue('entity', NULL)) {
      $sources = $form_state->getValue('entity');
      foreach ($sources as $key => $value) {
        if (!empty($value)) {
          $nodeBundles = $awsHelper->getBundles($value);
          $form['index']['entity_container'][$value] = [
            '#type' => 'checkboxes',
            '#prefix' => '<div class="entity_bundles accordion-toggle ' . $value . '">',
            '#suffix' => '</div>',
            '#title' => $this->t('Configure the %datasource datasource', ['%datasource' => $awsHelper->getEntityLabel($value)]),
            '#options' => $nodeBundles,
            '#required' => TRUE,
            '#default_value' => $config->get('bundles'),
          ];
        }
      }
    }
    $link = Link::createFromRoute(
      $this->t('<br><strong>Cancel</strong>'), 'aws_cloudsearch.index')->toString();
    unset($form['actions']);
    $form['submit'] = [
      '#type' => 'submit',
      '#weight' => 10,
      '#value' => $this->t('Submit'),
      '#attributes' => ['class' => ['button button--primary js-form-submit form-submit']],
    ];
    $form['edit'] = [
      '#markup' => $link,
  ];
    /*
    $blocks = $this->entityManager->getStorage('block_content_type')
        ->loadMultiple();
    foreach ($blocks as $cType) {
        $locksArr[$cType->get('id')] = $cType->get('label');
        //echo "<pre>"; print_r($cType->label()); echo "</pre>";
      //kint($cType);
      }
    kint('blocks', $locksArr);
    kint($content_entity_types);
    
    $contentTypes = $awsHelper->getContentTypes();
    $config = $this->config('aws_cloudsearch.index_config');
    if (isset($config)) {
      $config = $config->get('content_types');
    }
    $vocabList = $awsHelper->getVocabulary();
    $form['index']['node_type'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Content Type'),
      '#options' => $contentTypes,
      '#default_value' => (isset($config['type'])) ? $config['type'] : NULL,
    ];
    $form['index']['taxonomy_vocabulary'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Taxonomy'),
      '#options' => $vocabList,
      '#default_value' => (isset($config['type'])) ? $config['type'] : NULL,
    ];
    $form['index']['comment_type'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Content Type'),
      '#options' => $contentTypes,
      '#default_value' => (isset($config['comments'])) ? $config['comments'] : NULL,
    ];*/
    return $form;
  }
  
  /**
   * Handles changes to the selected datasources.
   *
   * @param array $form
   *   The current form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return array
   *   The part of the form to return as AJAX.
   */
  public function fetchDataSource(array $form, FormStateInterface $form_state) {
    return $form['index']['entity_container'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $awsHelper = AwsHelper::getInstance();
    $name = $form_state->getValue('name');
    $bundles = $form_state->getValue('entity_container');
//    kint($form_state->getValue('entity'));
//    kint($form_state->getValues());
//    kint($bundles);
//    die;
    // Check index name is exists or not.
    if ($awsHelper->getIndexes(['name' => $name]) != FALSE) {
      $form_state->setErrorByName('name', $this->t('Index name <strong>@name</strong> already exists.', ['@name' => $name]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $awsHelper = AwsHelper::getInstance();
    $name = $form_state->getValue('name');
    $domain_id = $form_state->getValue('domain_id');
    $entity_container = $form_state->getValue('entity_container');
    $entity_container = $awsHelper->getSelectedTypes($entity_container);
    $data = [];
    $data['name'] = $name;
    $data['domain_id'] = $domain_id;
    $data['entities'] = $entity_container;
    //$data['entity_bundles'] = $dataSource;
    // Save domain.
    $result = $awsHelper->saveIndex($data);
    // On success set success message and redirect Form.
    if ($result['status'] == 'success') {
      drupal_get_messages('status');
      drupal_set_message($result['message'], 'status');
      $form_state->setRedirectUrl(Url::fromRoute('aws_cloudsearch.index'));
    }
    else {
      drupal_set_message($result['message'], 'error');
    }
  }

}
