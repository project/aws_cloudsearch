<?php

namespace Drupal\aws_cloudsearch\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\TranslationManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\aws_cloudsearch\Helper\AwsHelper;
use Drupal\aws_cloudsearch\Controller\DomainController;
use Drupal\Core\Link;

/**
 * Class IndexForm.
 */
class IndexForm extends ConfigFormBase {

  /**
   * Drupal\Core\StringTranslation\TranslationManager definition.
   *
   * @var \Drupal\Core\StringTranslation\TranslationManager
   */
  protected $stringTranslation;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new DefaultForm object.
   */
  public function __construct(
  TranslationManager $string_translation,
      RequestStack $request_stack
  ) {
    $this->stringTranslation = $string_translation;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'aws_cloudsearch.index_config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('string_translation'), $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'index_list_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $add_link = Link::createFromRoute($this->t('Add Index'), 'aws_cloudsearch.index_add')->toString();
    $awsHelper = AwsHelper::getInstance();
    $results = $awsHelper->getIndexes();
    // Get Domain List in array format.
    $domain = new DomainController();
    $domainsList = $awsHelper->getDomains();
    $domainsList = $domain->converToArray($domainsList);
    $form['config-list'] = [
      '#type' => 'table',
      '#header' => [
        'index' => $this->t('Index Name'),
        'domain' => $this->t('AWS Domain & Region'),
        'datasource' => $this->t('Data Source'),
        'actions' => $this->t('Actions'),
      ],
      '#empty' => $this->t('There are no items yet @add_link.', ['@add_link' => $add_link]),
    ];
    foreach ($results as $result) {
      // Get index data for each index_id.
      $indexDataResult = $awsHelper->getIndexData(['index_id' => $result->id]);
      $dataSource = NULL;
      foreach ($indexDataResult as $indexData) {
        // Create string with entity and bundles.
        $dataSource .= $indexData->entity . ' => ' . implode(', ', unserialize($indexData->bundles)) . "<br>";
      }
      $form['config-list'][$result->id]['name'] = [
        '#markup' => $result->name,
      ];
      $form['config-list'][$result->id]['domain'] = [
        '#markup' => $domainsList[$result->domain_id],
      ];
      $form['config-list'][$result->id]['datasource'] = [
        '#markup' => $dataSource,
      ];
      $edit_link = Link::createFromRoute(
              $this->t('Edit'), 'aws_cloudsearch.index_edit', ['id' => $result->id]
          )->toString();
      $delete_link = Link::createFromRoute($this->t('Delete'), 'aws_cloudsearch.index_delete', ['id' => $result->id])->toString();
      $form['config-list'][$result->id]['actions'] = [
        '#markup' => $edit_link . ' | ' . $delete_link,
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
