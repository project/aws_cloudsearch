<?php

/**
 * @file
 * Contains \Drupal\aws_cloudsearch\Controller\DomainController.
 */

namespace Drupal\aws_cloudsearch\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Defines a controller to serve domain.
 */
class DomainController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'aws_cloudsearch.index_config',
    ];
  }

  /**
   * Get all saved domains.
   *
   * @return array
   *    Return domain list.
   */
  public function getDomains() {
    $config = $this->config('aws_cloudsearch.domain_config');
    return $config->get('domain_config');
  }

  /**
   * Convert two dimension array to single.
   *
   * @param array $configValue
   *    Multi dimension array.
   *
   * @return array
   *    Single dimension array.
   */
  public function converToArray($results) {
    $return = [];
    foreach ($results as $result) {
      $return[$result->id] = $result->name;
    }
    return $return;
  }

}
