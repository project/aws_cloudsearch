<?php

/**
 * @file
 * Contains \Drupal\aws_cloudsearch\Controller\IndexController.
 */

namespace Drupal\aws_cloudsearch\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Defines a controller to serve index.
 */
class IndexController extends ControllerBase {

}
