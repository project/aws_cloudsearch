<?php

namespace Drupal\aws_cloudsearch\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\TranslationManager;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class AwsController.
 */
class AwsController extends ControllerBase {

  /**
   * String Translation handler.
   *
   * @var Drupal\Core\StringTranslation\TranslationManager
   */
  private $translator;

  /**
   * Request handler object.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  private $request;

  /**
   * Entity manager handler.
   *
   * @var Drupal\Core\Entity\EntityManager
   */
  private $entityManager;


  /**
   * Constructs a new AwsConfig object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
      TranslationManager $string_translation,
    RequestStack $request_stack,
    $entityManager) {
    parent::__construct($config_factory);
    $this->translator = $string_translation;
    $this->request = $request_stack;
    $this->entityManager = $entityManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('string_translation'),
      $container->get('request_stack'),
      $container->get('entity.manager')
    );
  }

  /**
   * Translate string.
   *
   * @param string $string
   *   Translate string.
   *
   * @return string
   *   Return translated string.
   */
  public function translate($string) {
    return $this->translator->translate($string);
  }

  /**
   * Getawssearch.
   *
   * @return string
   *   Return Hello string.
   */
  public function getAwsSearch() {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: getAwsSearch')
    ];
  }

  /**
   * Searchcontent.
   *
   * @return string
   *   Return Hello string.
   */
  public function searchContent() {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: searchContent')
    ];
  }

  /**
   * Substrwords.
   *
   * @return string
   *   Return Hello string.
   */
  public function substrwords() {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: substrwords')
    ];
  }

  /**
   * Ingestcontent.
   *
   * @return string
   *   Return Hello string.
   */
  public function ingestContent() {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: ingestContent')
    ];
  }

}
