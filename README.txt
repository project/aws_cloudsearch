AWS Cloudsearch engine to search content. 
This module will work with only AWS 2.* version.

Installation: 
Step1: Install AWS vendor via composer:
        $ composer require aws/aws-sdk-php:2.*
        Run above command on the project's root folder.
Step2: Install aws_cloudsearch module via drush or UI.