<?php

/**
 * @file
 * Custom configuration.
 */

use Drupal\Core\Database\Database;

/**
 * AWS Cloudsearch dynamic table creation.
 *
 * Implements hook_schema()
 */
function aws_cloudsearch_schema() {
  $uniqueColumn = ['name'];
  $schema['aws_domain'] = [
    'fields' => [
      'id' => [
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'name' => [
        'type' => 'varchar',
        'length' => 150,
        'not null' => TRUE,
      ],
      'status' => [
        'type' => 'int',
        'size' => 'small',
        'not null' => TRUE,
      ],
      'created_at' => [
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
      ],
      'updated_at' => [
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
      ],
    ],
    'unique keys' => ['domain_name' => $uniqueColumn],
    'primary key' => ['id'],
  ];
  $schema['aws_index'] = [
    'fields' => [
      'id' => [
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'domain_id' => [
        'type' => 'int',
        'length' => 10,
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'name' => [
        'type' => 'varchar',
        'length' => 50,
        'not null' => TRUE,
      ],
      'status' => [
        'type' => 'int',
        'size' => 'small',
        'not null' => TRUE,
      ],
      'created_at' => [
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
      ],
      'updated_at' => [
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
      ],
    ],
    'unique keys' => ['index_name' => $uniqueColumn],
    'primary key' => ['id'],
  ];
  $schema['aws_index_data'] = [
    'fields' => [
      'id' => [
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'index_id' => [
        'type' => 'int',
        'length' => 10,
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'entity' => [
        'type' => 'varchar',
        'length' => 100,
        'not null' => TRUE,
      ],
      'bundles' => [
        'type' => 'text',
        'not null' => TRUE,
      ],
      'created_at' => [
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
      ],
      'updated_at' => [
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
      ],
    ],
    'primary key' => ['id'],
  ];
  $schema['aws_index_fields'] = [
    'fields' => [
      'id' => [
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'fields_source' => [
        'type' => 'text',
        'not null' => TRUE,
      ],
      'status' => [
        'type' => 'int',
        'size' => 'small',
        'not null' => TRUE,
      ],
      'created_at' => [
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
      ],
      'updated_at' => [
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
      ],
    ],
    'primary key' => ['id'],
  ];
  return $schema;
}

/**
 * Update schema on module update.
 *
 * Implements hook_update_N()
 */
function aws_cloudsearch_update_1() {
  // Update goes here.
}
